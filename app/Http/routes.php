<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['prefix' => 'api', 'namespace' => 'Api'], function()
{
    Route::get('comments', 'CommentController@index');
    Route::get('comment/rand', 'CommentController@rand');
    Route::post('comment/new', 'CommentController@store');
});
Route::get('/{vue_capture?}', function () {
    return view('layouts.app');
})->where('vue_capture', '[\/\w\.-]*');;
