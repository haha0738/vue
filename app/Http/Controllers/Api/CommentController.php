<?php

namespace App\Http\Controllers\Api;

use App\Services\Comment\CommentRepositoryInterface;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class CommentController extends Controller
{
    private $comments;
    public function __construct(CommentRepositoryInterface $comments)
    {
        $this->comments = $comments;
    }
    public function index()
    {
        $comments = $this->comments->paginate(5);
        return response()->json($comments);
    }

    public function store(Requests\CommentStoreRequest $request)
    {
        $comment = $this->comments->create($request->all());
        return response()->json(['success' => true, 'data' => $comment]);
    }

    public function rand()
    {
        $comment = $this->comments->rand();
        return response()->json($comment);
    }


}
