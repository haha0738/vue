<?php
/**
 * Created by PhpStorm.
 * User: haha0738
 * Date: 2016/7/13
 * Time: 下午11:56
 */

namespace App\Services\Comment;


use App\Models\Comment;

class CommentRepository implements CommentRepositoryInterface
{
    private $comment;
    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }
    public function paginate($page)
    {
        return $this->comment->query()->paginate($page);
    }

    public function create($data)
    {
        return Comment::create($data);
    }

    public function rand()
    {
        $count = $this->comment->query()->count();
        $index= floor(rand()/getrandmax() * $count);
        return $this->comment->query()->where('id', '>=', $index)->first();
    }
}