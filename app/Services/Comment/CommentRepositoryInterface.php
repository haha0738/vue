<?php
namespace App\Services\Comment;
interface CommentRepositoryInterface
{
    function paginate($page);
    function rand();
    function create($data);
}