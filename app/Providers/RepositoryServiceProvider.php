<?php

namespace App\Providers;

use App\Models\Comment;
use App\Services\Comment\CommentRepository;
use App\Services\Comment\CommentRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CommentRepositoryInterface::class, function($app)
        {
           return new CommentRepository(new Comment());
        });

    }
}
