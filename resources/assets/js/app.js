/**
 * require============================================================================
 */
window.$ = window.jQuery = require('jQuery');
require('bootstrap-sass');
var Vue = require('vue');
/**
 * routes
 */
var VueRouter = require('vue-router');
/**
 * i18n
 */
var VueI18n = require('vue-i18n');
/**
 * resource
 */
var VueResource = require('vue-resource');
/**
 * validator
 */
var VueValidator = require('vue-validator')
/**
 * ==================================================================================
 */

/**
 * config=================================================================================
 */
var config = require('./vue/config/config.js');

console.log(config);

/**
 * =======================================================================================
 */
Vue.use(VueRouter);

Vue.use(VueI18n);
Vue.config.lang = 'zh_TW';

Vue.use(VueResource);
Vue.http.options.root = '/api';
console.log($('#csrf_token').attr('content'));
Vue.http.headers.common['X-CSRF-TOKEN'] = $('#csrf_token').attr('content');

Vue.use(VueValidator);
Object.keys(config.locales).forEach(function (lang) {
    Vue.locale(lang, config.locales[lang])
});

var App = require('./vue/components/app.vue');

var router = new VueRouter(config.route);

router.map({
    '/about': {
        component: require('./vue/components/about.vue'),
    },
    '/': {
        component: require('./vue/components/home.vue'),
    },
    '/comment/new': {
        name: 'new-comment',
        component: require('./vue/components/new-comment.vue'),
    },
    '/comment/:page': {
        name: 'comment',
        component: require('./vue/components/comment.vue'),
    },
    '/dine': {
        component: require('./vue/components/dine.vue'),
    }
});

router.start(App, '#app');


