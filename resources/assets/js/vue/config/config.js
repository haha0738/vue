var route = (require('./routes.js'));
var locales = (require('./locales.js'));
module.exports = {
    route: route.route,
    locales: locales.locales,
}
