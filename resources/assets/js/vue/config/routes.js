/**
 * route
 * @type {{hashbang: boolean, history: boolean, mode: string, linkActiveClass: string, transitionOnLoad: boolean, root: string}}
 */


exports.route= {
    hashbang: false,
    //abstract: true,
    history: true,
    mode: 'html5',
    linkActiveClass: 'active',
    transitionOnLoad: true,
    root: '/'
};