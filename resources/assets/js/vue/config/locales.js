/**
 *i18n
 * @type {{zh_TW: {date: {days: string, hours: string, minutes: string, seconds: string}}, en: {date: {days: string, hours: string, minutes: string, seconds: string}}}}
 */
module.exports.locales = {
    zh_TW: {
        date: {
            days: '天',
            hours: '時',
            minutes: '分',
            seconds: '秒',
            countdown: '倒數 ',
            passed: '已離職 ',
        },
        nav: {
            home: '首頁',
            about: '關於我',
            contact: '聯絡方式',
            dine: '聚餐',
            comment: '留言',
        },
        page: {
            loading: '頁面載入中....',
        },
        comment: {
            title: '留言',
            from: '留言人',
            created_at: '留言時間',
        }
    },
    en: {
        date: {
            days: 'days',
            hours: 'hours',
            minutes: 'minutes',
            seconds: 'seconds',
            countdown: 'countdown ',
            passed: 'passed ',
        },
        nav: {
            home: 'Home',
            about: 'About',
            contact: 'Contact',
            dine: 'dine',
            comment: 'comment',
        },
        page: {
            loading: 'Loading....',
        }
    }
}