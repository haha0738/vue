<!DOCTYPE HTML>
<html>
<head>
	<title>緬懷 - made by 豆花</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta id="csrf_token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
	<video autoplay loop poster="{{asset('images/doraemon.jpg')}}" id="bgvid">
	    <source src="{{asset('video/vedio.mp4')}}" type="video/mp4">
	</video>
    <div id="app">
    </div>
    <script src="{{asset('js/app.js')}}"></script>
</body>
</html>